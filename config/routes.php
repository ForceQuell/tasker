<?php

return array(

'taskList/([0-9]+)/([a-z]+)' => 'task/list/$1/$2',
'taskList/([0-9]+)' => 'task/list/$1',
'taskList' => 'task/list',
'newTask' => 'task/create',
'admin/out' => 'admin/out',
'admin/([0-9]+)/([a-z]+)'=> 'admin/auth/$1/$2',
'admin/([0-9]+)'=> 'admin/auth/$1',
'admin' => 'admin/auth',
'default' => 'default/def'
);