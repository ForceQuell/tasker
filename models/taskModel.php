<?php
class TaskModel {
	
	public static function taskList($pageNum, $sort, $dir) {
		$n = NUMBER_OF_ELEMENTS_ON_PAGE;
		$db = Db::dbConnect();
		
		$request = "SELECT COUNT(*) FROM tasks";
		if($result = $db->query($request)) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$nTasks = $row['COUNT(*)'];
			$result->close();
		}
		
		$pages = intval($nTasks / $n);
		if($nTasks % $n != 0)
			$pages++;
		
		if($pageNum > $pages)
			$pageNum = $pages;
		if($pageNum < 1)
			$pageNum = 1;
		
		$x = $n*($pageNum-1);
		$y = $x+$n;
		
		$request = "SELECT id, user, mail, task, status, image FROM tasks ORDER BY $sort $dir LIMIT $x,$y";
		if($result = $db->query($request)) {
			for($i = 0; $i < $n; $i++) {
				$data[$i] = $result->fetch_array(MYSQLI_ASSOC);
			}
			$result->close();
		}
		
		return array('pages' => $pages, 'currentPage' => $pageNum, 'data' => $data);
	}
	
	public static function taskCreate($data) {
		$types = array(
			'image/gif',
			'image/png',
			'image/jpeg',
			'image/jpg'
		);
		$kbytes = 500;
		$size = 1024*$kbytes; //bytes
		$wMax= 320;
		$hMax = 240;
		
		
		if(!in_array($data['picture']['type'], $types)) {
			echo '<br>Запрещенный тип файла!<br>';
			die();
		}
		
		if($data['picture']['size'] > $size) {
			echo '<br>слишком большой файл! максимальный размер файла '.$kbytes.'КБ<br>';
			die();
		}
		
		try {
			if ($data['picture']['type'] == 'image/jpeg')
				$src = imagecreatefromjpeg($data['picture']['tmp_name']);
			elseif ($data['picture']['type'] == 'image/png')
				$src = imagecreatefrompng ($data['picture']['tmp_name']);
			elseif ($data['picture']['type'] == 'image/gif')
				$src = imagecreatefromgif ($data['picture']['tmp_name']);
			else
				return false;
		}
		catch(Exeption $e) {
			echo 'неизвестная ошибка (возможно изображние слишком большое)';
			die();
		}
		
		$wOrig = imagesx($src); 
		$hOrig = imagesy($src); //echo '<br>'.$wOrig.'x'.$hOrig.'<br>';
		$k = 1;
		if($wOrig > $wMax || $hOrig > $hMax) {
			$k1 = $wMax / $wOrig;
			$k2 = $hMax / $hOrig;
			$k = $k1 < $k2 ? $k1 : $k2;
		}
		
		$wRes = ceil($wOrig * $k);
		$hRes = ceil($hOrig * $k); //echo '<br>'.$wRes.'x'.$hRes.'<br>';
		$resImg = imagecreatetruecolor($wRes, $hRes);
		
		imagecopyresampled($resImg, $src, 0, 0, 0, 0, $wRes, $hRes, $wOrig, $hOrig);
		imagejpeg($resImg, ROOT.'/images/'.$data['picture']['name']);
		imagedestroy($resImg);
		imagedestroy($src);
		
		$db = Db::dbConnect();
		
		$data['user'] = $db->escape_string($data['user']);
		$data['mail'] = $db->escape_string($data['mail']);
		$data['task'] = $db->escape_string($data['task']);
		
		$request ="INSERT INTO tasks (user, mail, task, status, image)".
		" VALUES ('".$data['user']."', '".$data['mail']."', '".$data['task']."', '0', '".$data['picture']['name']."')";
		
		if($result = $db->query($request)) {
		}
		else {
		}
	}
}