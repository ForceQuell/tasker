<?php
	//print_r($data['data']);
?>

<div class="col-sm-4">
	<h4><?php echo $arr['user'];?> 
	<small>(<a href="mailto:<?php echo $arr['mail'];?>"><?php echo $arr['mail'];?></a>)</small>
	<input type="checkbox" <?php if($arr['status']) echo 'checked="checked"'; ?> disabled>
	</h4>
	<?php if($arr['image']) { ?>
		<img class="center-block img-rounded center-block" src="<?php echo '/images/'.$arr['image']; ?>" alt="no image">
	<?php } ?>
	<p><?php echo $arr['task']; ?></p>
</div>
