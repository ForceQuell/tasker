<div class="col-sm-4"> <!-- ADMINISTRATOR -->
	<h4><?php echo $arr['user'];?> 
	<small>(<a href="mailto:<?php echo $arr['mail'];?>"><?php echo $arr['mail'];?></a>)</small>
	<input type="checkbox" name="check" value="<?php echo $arr['id']; ?>" <?php if($arr['status']) echo 'checked="checked"'; ?>>
	</h4>
	<?php if($arr['image']) { ?>
		<img class="center-block img-rounded center-block" src="<?php echo '/images/'.$arr['image']; ?>" alt="no image">
	<?php } ?>
	<textarea name="area" data-id="<?php echo $arr['id']; ?>"><?php echo $arr['task']; ?></textarea>
</div>
