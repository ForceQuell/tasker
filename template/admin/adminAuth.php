<html>
	<head>
		<title>Admin authorization</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<style>
			html, body {
				width: 100%;
				height: 100%;
				margin: 0;
			}
			.auth {
				position: absolute;
				float: none;
				height: 14em;
				width: 20em;
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;
				margin: auto auto;
				box-shadow: #000 0px 0px 10px;
				padding: 10px;
				border-radius: 10px;
			}
		</style>
	</head>

	<body>
		<div class="auth">
			<h2 class="text-center">Авторизация</h3>
			<form action="/admin" method="POST" class="text-center">
				<input type="text" name="login" placeholder="login">
				<input type="password" name="password" placeholder="password"><br>
				<input type="submit" value="вход">
			</form>
			<?php
			if(!$authorizated) {
				?>
				<h4 class="text-center">Неверный пароль/логин!</h4>
				<?php
			}
		?>
		</div>
	</body>
</html>