<!DOCTYPE html>
<html lang="en">
<head>
	<title>TASKER</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<style>
		/* Custom navbar-center*/
		@media (min-width: 768px) {
			.navbar-center {
				position: absolute;
				left: 0;
				right: 0;
				width: 250px;
				margin: auto auto;
			}
		}
		p {
			text-indent: 1em;
		}
	</style>
	
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="/">Tasker</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-left">
        <li><a href="/newTask">New</a></li>
		<li class="btn-group">
			<a class="btn dropdown-toggle" data-toggle="dropdown" href="">Sort <i class="caret"></i></a>
			<ul class="dropdown-menu">
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/auser">user <i class="glyphicon glyphicon-arrow-up"></i></a></li>
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/duser">user <i class="glyphicon glyphicon-arrow-down"></i></a></li>
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/amail">e-mail <i class="glyphicon glyphicon-arrow-up"></i></a></li>
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/dmail">e-mail <i class="glyphicon glyphicon-arrow-down"></i></a></li>
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/astatus">status <i class="glyphicon glyphicon-arrow-up"></i></a></li>
				<li><a href="/taskList/<?php echo $data['currentPage'];?>/dstatus">status <i class="glyphicon glyphicon-arrow-down"></i></a></li>
			</ul>
		</li>
      </ul>
      <ul class="nav navbar-nav navbar-center">
	  
        <li><a href="<?php if($data['currentPage']-1 > 0) echo '/taskList/'.($data['currentPage']-1).'/'.$data['sort']; ?>">Page-</a></li>
		<li><a>Page <?php echo ($data['pages']? $data['currentPage'] : 0); ?> of <?php echo $data['pages']; ?> </a></li>
		<li><a href="<?php if($data['currentPage'] < $data['pages']) echo '/taskList/'.($data['currentPage']+1).'/'.$data['sort']; ?>">Page+</a></li>
		
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/admin">Admin</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
	<h3 class="text-center"><?php if($data['pages']) echo 'Tasks'; else echo 'No tasks here';?></h3>
	<div class="row">
