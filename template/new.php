<html>
<head>
	<title>New task</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<style>
		img {
			max-width: 320px;
			max-heigth: 240px;
		}
		p {
			text-indent: 1em;
		}
		#preview {
			display: none;
		}
		/* Custom navbar-center*/
		
		@media (min-width: 768px) {
			.newTask .navbar-center {
				position: absolute;
				left: 0;
				right: 0;
				width: 88px;
				margin: auto auto;
			}
		}
	</style>

</head>

<body class="newTask">
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="/">Tasker</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-left">
        <li><a href="/newTask">New</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-center">
        <li><a href="/">TasksList</a></li>
      </ul>
    </div>
  </div>
</nav>

<div>
	<div id="preview">
	</div>
</div>

<form method="POST" action="/newTask" enctype="multipart/form-data" class="col-sm-4">
	<label>User: <input type="text" name="user" id="user" placeholder="user" required></label><br>
	<label>Mail: <input type="email" name="mail" id="mail" placeholder="mail" required></label><br>
	<label>Image: <input type="file" name="picture" accept="image/jpeg,image/png,image/gif" onchange="pictureLoader(files[0]);"></label><br>
	<label><textarea name="task" id="task" placeholder="your task" rows="15" cols="45" required></textarea></label><br>
	<input type="button" value="Preview" id="prev" class="btn btn-primary"> <input type="submit" value="Создать" class="btn btn-primary"> <input type="reset" class="btn btn-primary">
</form>

<script src="/js/preview.js"></script>

</body>
</html>