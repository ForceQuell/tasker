<?php

class TaskView {
	
	public static function listView($data) {
		
		include(ROOT.'/template/skeleton_1.php');

		foreach ($data['data'] as $arr) {
			if($arr)
				include(ROOT.'/template/item.php');
		}
		
		include(ROOT.'/template/skeleton_2.php');
	}
	
	public static function taskCreate() {
		include(ROOT.'/template/new.php');
	}
}
