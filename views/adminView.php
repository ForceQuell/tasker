<?php

class AdminView {
	
	public static function auth($authorizated = 1) {
		include(ROOT.'/template/admin/adminAuth.php');
	}
	
	public static function listView($data) {
		
		include(ROOT.'/template/admin/skeleton_1.php');

		foreach ($data['data'] as $arr) {
			if($arr)
				include(ROOT.'/template/admin/item.php');
		}
		
		include(ROOT.'/template/admin/skeleton_2.php');
	}
	
}

