<?php //MAIN

error_reporting(E_ALL);

define('ROOT', dirname(__FILE__)); //full path
define('NUMBER_OF_ELEMENTS_ON_PAGE', 3);
require_once(ROOT.'/components/Router.php');
require_once(ROOT.'/components/Db.php');

$router = new Router();
$router -> run();