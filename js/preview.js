var img = new Image();
var template = '<div class="col-sm-4"><h4>%USER% <small>(%EMAIL%)</small> <input type="checkbox" disabled></h4>%IMAGEBODY%<p>%TEXT%</p></div>';

function pictureLoader(fileData) {
	img.onload = function() {
		var width = img.width;
		var height = img.height;
	}
	img.src = URL.createObjectURL(fileData);
}

function replaceNode(from, to) {
	var parent = from.parentNode;
	to.id = from.id;
	parent.removeChild(from);
	parent.appendChild(to);
}

prev.onclick = function() {
	
	if (user.value === "" || mail.value === "") {
		alert('Incorrect data specified');
		return;
	}
	
	var div = document.createElement("div");
	var inner = template;

	if(img.src) {
		inner = inner.replace('%IMAGEBODY%', '<img class="center-block img-rounded center-block" src="%IMAGE%" alt="no image">');
	} else {
		inner = inner.replace('%IMAGEBODY%', '');
	}
	
	div.innerHTML = inner.replace('%USER%', user.value.replace(/%/g, '')).replace('%EMAIL%', mail.value.replace(/%/g, '')).replace('%IMAGE%', img.src || "").replace('%TEXT%', task.value.replace(/%/g, ''));
	
		
	div.style.display = "block";
	
	replaceNode(preview, div);
}