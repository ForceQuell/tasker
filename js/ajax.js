function ajax(type, url, arguments, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open(type, url);
    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                callback(this.responseText);
            } else {
                console.error('Server returns a '+ this.status + ' status code. URL: "'+ url +'"');
                alert('Unexpected Error. ['+ this.status +']');
            }
        }
    };
	xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send((arguments?arguments:null));
}