
document.addEventListener("DOMContentLoaded", function(event) {
	document.querySelectorAll('input[name="check"]').forEach(function(element){
		element.addEventListener('change', function() {
			ajax('POST', '/admin', 'id='+element.value, function(){
				var div = document.createElement('div');
				div.className = 'OK';
				div.innerText = 'saved';
				document.body.appendChild(div);
				setTimeout(function(){
					document.body.removeChild(div);
				}, 3000);
			});
		});
	});
});

document.addEventListener("DOMContentLoaded", function(event) {
	document.querySelectorAll('textarea[name="area"]').forEach(function(element){
		element.addEventListener('blur', function(){
			ajax('POST', '/admin', 'task='+element.value+'&number='+element.getAttribute('data-id'), function(){
				var div = document.createElement('div');
				div.className = 'OK';
				div.innerText = 'saved';
				document.body.appendChild(div);
				setTimeout(function(){
					document.body.removeChild(div);
				}, 3000);
			});
		});
	});
});