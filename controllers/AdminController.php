<?php
require_once(ROOT.'/views/AdminView.php');
require_once(ROOT.'/models/adminModel.php');
require_once(ROOT.'/components/AuthClass.php');

session_start();

class AdminController {
	
	public function actionAuth($pageNum = 1, $sort = 'dstatus') {
		$auth = new AuthClass();
		
		if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:  
			if(isset($_POST['out'])) {
				$auth->out();
				AdminView::auth();
			}
			else {
				if(isset($_POST['id']))
					AdminModel::changeStatus(intval($_POST['id']));
				if(isset($_POST['task']))
					AdminModel::changeTask(intval($_POST['number']), $_POST['task']);
				else
					$this->actionList(intval($pageNum), $sort);
			}
				
		}
		else{
			if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
				
				if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно
					AdminView::auth(0);
				}
				else {
					$this->actionList($pageNum, $sort);
				}
			}
			else {
				AdminView::auth();
			}
		}
		
		
		
		return true;
	}
	
	public function actionOut() {
		AuthClass::out();
		header('Location:/');
	}
	
	private function actionList($pageNum, $sort) {
		$_sort = $sort;
		$dir = $sort[0];
		if($dir === 'd')
			$dir = 'DECS';
		if($dir === 'a')
			$dir = 'ASC';
		else
			$dir = 'DESC';
		
		$sort = substr_replace($sort, '', 0, 1);
		if( $sort !== 'status' && $sort !== 'user' && $sort !== 'mail') {
			$sort = 'status';
			$dir = 'DECS';
		}
		
		$data = AdminModel::taskList($pageNum, $sort, $dir);
		
		$data['sort'] = $_sort;
		//view
		AdminView::listView($data);
		
		return true;
	
	}
}