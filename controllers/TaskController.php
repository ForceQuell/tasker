<?php

require_once(ROOT.'/models/TaskModel.php');
require_once(ROOT.'/views/TaskView.php');

class TaskController {
	
	public function actionList($pageNum = 1, $sort = 'dstatus') {
		
		$_sort = $sort;
		$dir = $sort[0];
		if($dir === 'd')
			$dir = 'DECS';
		if($dir === 'a')
			$dir = 'ASC';
		else
			$dir = 'DESC';
		
		$sort = substr_replace($sort, '', 0, 1);
		if( $sort !== 'status' && $sort !== 'user' && $sort !== 'mail') {
			$sort = 'status';
			$dir = 'DECS';
		}
		
		$data = TaskModel::taskList(intval($pageNum), $sort, $dir);
		
		$data['sort'] = $_sort;
		//view
		TaskView::listView($data);
		
		return true;
	}
	
	public function actionCreate() {
		
		if(!isset($_POST['task']))
			TaskView::taskCreate();
		
		else {
			$data = Array(
				'user' => $_POST['user'],
				'mail' => $_POST['mail'],
				'task' => $_POST['task'],
				'picture' => $_FILES['picture']
			);
			
			TaskModel::taskCreate($data);
			
			$this->actionList();
		}
		
		return true;
	}
}